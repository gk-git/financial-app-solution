import React from 'react';
import Header from "./component/Header";
import Footer from "./component/Footer";

import './App.scss';

function App() {
  return (
    <div className="App">
      <Header/>
      Hello
      <Footer/>
    </div>
  );
}

export default App;
