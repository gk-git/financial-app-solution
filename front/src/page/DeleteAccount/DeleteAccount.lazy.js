import React, { lazy, Suspense } from 'react';

const LazyDeleteAccount = lazy(() => import('./DeleteAccount'));

const DeleteAccount = props => (
  <Suspense fallback={null}>
    <LazyDeleteAccount {...props} />
  </Suspense>
);

export default DeleteAccount;
