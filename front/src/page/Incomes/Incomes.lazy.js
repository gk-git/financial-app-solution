import React, { lazy, Suspense } from 'react';

const LazyIncomes = lazy(() => import('./Incomes'));

const Incomes = props => (
  <Suspense fallback={null}>
    <LazyIncomes {...props} />
  </Suspense>
);

export default Incomes;
