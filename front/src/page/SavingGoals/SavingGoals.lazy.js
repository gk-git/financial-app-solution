import React, { lazy, Suspense } from 'react';

const LazySavingGoals = lazy(() => import('./SavingGoals'));

const SavingGoals = props => (
  <Suspense fallback={null}>
    <LazySavingGoals {...props} />
  </Suspense>
);

export default SavingGoals;
